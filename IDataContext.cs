﻿/* Copyright Jarle Moe 2014
 * 
 * http://www.apache.org/licenses/LICENSE-2.0.html
 * 
 */
using Cuddlefluff.Data.Cache;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuddlefluff.Data
{
    public interface IDataContext
    {
        IDbConnection Connection { get; }
        ICache Cache { get; }

        IDbConnection GetConnection(object connectionKey);

        void OnCreated(object sender, object entity);
        void OnUpdated(object sender, object entity);
        void OnDeleted(object sender, object entity);
        void OnException(object sender, object entity, Exception ex);

    }
}
