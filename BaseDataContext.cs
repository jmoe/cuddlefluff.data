﻿/* Copyright Jarle Moe 2014
 * 
 * http://www.apache.org/licenses/LICENSE-2.0.html
 * 
 */
using Cuddlefluff.Data.Cache;
using Cuddlefluff.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuddlefluff.Data
{
    /// <summary>
    /// Base data context. This class should contain all methods necessary for working with the database
    /// </summary>
    public abstract class BaseDataContext : IDisposable, IDataContext
    {
        private readonly List<IDisposable> _createdRepositories;

        private readonly List<object> _repositories;

        private readonly Lazy<IDbConnection> _connection;
        private readonly Dictionary<string, IDbConnection> _connections;

        protected Dictionary<string, IDbConnection> Connections { get { return _connections; } }

        /// <summary>
        /// Gets the primary database connection this datacontext uses
        /// </summary>
        public IDbConnection Connection { get { return _connection.Value; } }

        /// <summary>
        /// Retrieves a connection by key.
        /// </summary>
        /// <param name="key">An object identifier for the connection</param>
        /// <returns></returns>
        public IDbConnection GetConnection(object key)
        {
            // the reason why it uses object as a key is so that you can use any type of object to retrieve a connection.
            // the idea is that you can provide an Enum to know which connection to use, so you don't get any magic strings
            if (key == null)
                return Connection;

            string keyName = key.ToString();

            if (Connections.ContainsKey(keyName))
                return Connections[keyName];

            var conn = CreateConnection(keyName);

            Connections.Add(keyName, conn);

            return conn;
        }

        protected abstract IDbConnection CreateConnection();

        protected virtual IDbConnection CreateConnection(string key)
        {
            throw new NotImplementedException();

            /* Example code :
             * You can retrieve connectionString and providerName from Web/Application.Config.
             * I won't do it here, because its use depends on implementation,
             * additionally I don't want any references that aren't strictly necessary.
            
            string connString = yourConfigSource[key].ConnectionString;
            string providerName = yourConfigSource[key].ProviderName;
            
            var factory = DbProviderFactories.GetFactory(providerName);

            var connection = factory.CreateConnection();

            connection.ConnectionString = connectionString;

            return connection.Open();*/
             
             
        }

        private readonly Lazy<ICache> _cache;

        /// <summary>
        /// Gets the cache this context uses
        /// </summary>
        public ICache Cache { get { return _cache.Value; } }

        /// <summary>
        /// Creates the cache object. In order to add caching this method must be overridden
        /// </summary>
        /// <returns></returns>
        protected virtual ICache CreateCache()
        {
            return new NoCache();
        }

        // override this if your cache is split into seperate stores (like Azure Cache)
        /// <summary>
        /// Returns a cache store
        /// </summary>
        /// <param name="storeName">Name of the cache store or configuration to use</param>
        /// <returns>Cache store or null of store cannot be found.</returns>
        public virtual ICache GetCacheStore(string storeName)
        {
            throw new NotImplementedException();
        }
        
        /// <summary>
        /// Instanciates a new DataContext. This constructor needs to be called in all extending classes
        /// </summary>
        protected BaseDataContext()
        {
            _createdRepositories = new List<IDisposable>();
            _repositories = new List<object>();

            _connection = new Lazy<IDbConnection>(() => CreateConnection());

            _cache = new Lazy<ICache>(() => CreateCache());

            _connections = new Dictionary<string, IDbConnection>();
        }

        /// <summary>
        /// Creats a lazy loaded repository
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected Lazy<T> CreateRepository<T>() where T : IDisposable, IRepository, new()
        {
            var result = new Lazy<T>(() =>
            {
                var repo = new T();
                
                repo.SetDataContext(this);
                
                if(repo is IInitialize)
                    ((IInitialize)(repo)).Initialize();

                _createdRepositories.Add(repo);

                return repo;
            });

            _repositories.Add(result);

            return result;
        }


        /// <summary>
        /// Closes the data connection and calls dispose on any loaded repositories
        /// </summary>
        public virtual void Dispose()
        {
            // Release repositories
            foreach(var repo in _createdRepositories )
            {
                repo.Dispose();
            }

            // dispose connection
            if (_connection.IsValueCreated)
                _connection.Value.Dispose();

            // dispose cache
            if (_cache.IsValueCreated)
                _cache.Value.Dispose();

            // dispose extra connections
            foreach (var entry in Connections.Values)
                entry.Dispose();

        }

        /// <summary>
        /// Event for when an entity has been created by a repository
        /// </summary>
        /// <param name="entity">Entity that has been created</param>
        public virtual void OnCreated(object sender, object entity)
        {

        }

        /// <summary>
        /// Event for when an entity has been updated by a repository
        /// </summary>
        /// <param name="entity"></param>
        public virtual void OnUpdated(object sender, object entity)
        { 

        }

        /// <summary>
        /// Event for when an entity has been deleted by a repository
        /// </summary>
        /// <param name="entity"></param>
        public virtual void OnDeleted(object sender, object entity)
        {

        }

        /// <summary>
        /// Event for when a repository throws an exception
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="ex"></param>
        public virtual void OnException(object sender, object entity, Exception ex)
        {

        }
    }
}
