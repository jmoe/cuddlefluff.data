﻿/* Copyright Jarle Moe 2014
 * 
 * http://www.apache.org/licenses/LICENSE-2.0.html
 * 
 */
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Cuddlefluff.Data.DataAnnotations;
using System.Data.Common;
using Cuddlefluff.Data.Cache;

namespace Cuddlefluff.Data
{


    public class BaseEntityRepository<TEntity> : IDisposable, IRepository where TEntity : new()
    {
        private IDataContext _dataContext = null;

        public IDataContext DataContext { get { return _dataContext; } }

        protected virtual IDbConnection Connection { get { return DataContext.Connection; } }

        public void SetDataContext(IDataContext context)
        {
            _dataContext = context;
        }

        /// <summary>
        /// Queries the database for the entity. This is a shortcut method, intended to create a space between dapper and the repository for any code insertions
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        protected IEnumerable<TEntity> QueryEntity(string sql, dynamic parameters = null, IDbTransaction transaction = null)
        {
            return Connection.Query<TEntity>(sql, (object)parameters, transaction);
        }

        /// <summary>
        /// Runs several queries and returns all resulting entities
        /// </summary>
        /// <param name="sql">An enumerable list of SQL statments to call</param>
        /// <param name="parameters"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        protected IEnumerable<TEntity> QueryEntity(IEnumerable<string> sql, dynamic parameters = null, IDbTransaction transaction = null)
        {
            IEnumerable<TEntity> res = Enumerable.Empty<TEntity>();

            foreach(var query in sql)
                res = res.Concat<TEntity>(QueryEntity(query, (object)parameters, transaction));

            return res;

        }

        protected IEnumerable<dynamic> Query(string sql, dynamic parameters = null, IDbTransaction transaction = null)
        {
            return Connection.Query(sql, (object)parameters, transaction);
        }

        protected IEnumerable<T> Query<T>(string sql, dynamic parameters = null, IDbTransaction transaction = null)
        {
            return Connection.Query<T>(sql, (object)parameters, transaction);
        }

        /// <summary>
        /// Execute an SQL script
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        protected int Execute(string sql, dynamic parameters = null, IDbTransaction transaction = null)
        {
            return Connection.Execute(sql, (object)parameters, transaction);
        }

        protected IEnumerable<int> Execute(IEnumerable<string> sql, dynamic parameters = null, IDbTransaction transaction = null)
        {
            foreach(var query in sql)
                yield return Execute(query, parameters, transaction);
        }

        /// <summary>
        /// Send a query to the database intended to retrieve a list of objects as a result.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> QueryCollection(string sql, dynamic parameters = null, bool cache = true, IDbTransaction transaction = null)
        {
            // cache?
            return Connection.Query<TEntity>(sql, (object)parameters, transaction);
        }

        protected static IEnumerable<string> GetProperties(Type type)
        {
            return type.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly).Select(a => a.Name);
        }

        public virtual TEntity CreateEntity()
        {
            return new TEntity();
        }

        #region Get single entity

        private const string GetEntityWhereFormat = " WHERE {0}";

        private const string GetEntitySelectSqlFormat = "SELECT * FROM [{0}] {1}";

        private const string GetEntityJoinSqlFormat = "INNER JOIN [{0}] ON {1}";

        protected string GetSelectStatement()
        {
            // Get the tables this entity is comprised of
            var tables = new List<string>(GetTableNames());

            // the first table will be the base entity
            var tname = tables.Last();


            return string.Format(GetEntitySelectSqlFormat,
                tname,
                string.Join(" ", tables.Where(a => !a.Equals(tname)).Select(s => string.Format(GetEntityJoinSqlFormat, s, string.Join(" AND ", GetKeys().Select(k => string.Format("[{1}].{2} = [{0}].{2}", tname, s, k)))))));

        }

        protected virtual TEntity GetEntityById(object id, IDbTransaction transaction = null)
        {
            TEntity result = GetCache(id);

            if (result != null)
                return result;

            // Get the tables this entity is comprised of
            var tables = new List<string>(GetTableNames());

            // the first table will be the base entity
            var tname = tables.Last();

            string query = 
                string.Concat(
                    GetSelectStatement(),
                    string.Format(
                        GetEntityWhereFormat,
                        string.Join(
                            " AND ",
                            GetKeys(false).Select(k => string.Format("[{0}].{1} = @{1}", tname, k))
                        )
                    )
                )
            ;

            // Make a query with inner joins and retrieve all fields from all entities
            result = QueryEntity(query, id, transaction).FirstOrDefault();

            UpdateCache(result);

            // TODO : maybe move this to LINQ by providing IQueryable interface on repositories

            return result;
        }

        #endregion

        #region Update

        private const string UpdateSqlFormat = "UPDATE [{0}] SET {1} WHERE {2}";

        protected virtual string BuildUpdateSql(EntityMutator<TEntity> mutator, string tableName, IEnumerable<string> properties)
        {

            var props = from entry in mutator.GetModifiedProperties() where properties.Contains(entry) select entry;

            if (props.Count() == 0) // no properties has been modified for this table
                return null;

            // Update Table SET A = @A, B = @B WHERE Key1 = @Key1 AND Key2 = @Key2
            return string.Format(UpdateSqlFormat, 
                tableName, 
                string.Join("," ,  props.Select(entry => string.Format("{0} = @{0}", entry))),
                string.Join(" AND ", GetKeys(false).Select(s => string.Format("{0} = @{0}", s)))
                );
        }

        public virtual IDbTransaction BeginTransaction()
        {
            return Connection.BeginTransaction();
        }

        protected virtual IEnumerable<string> BuildUpdateSql(EntityMutator<TEntity> mutator)
        {
            var entityType = typeof(TEntity);

            do
            {

                var tAttr = entityType.GetCustomAttributes(typeof(TableAttribute), false).FirstOrDefault() as TableAttribute;

                if (tAttr == null)
                    continue;

                var sql = BuildUpdateSql(mutator, tAttr.Name ?? entityType.Name, GetProperties(entityType));

                if (sql != null)
                    yield return sql;

                

            } while ((entityType = entityType.BaseType) != typeof(object));

        }



        protected virtual TEntity UpdateEntity(EntityMutator<TEntity> mutator, IDbTransaction transaction = null)
        {
            DynamicParameters parameters = new DynamicParameters(mutator.Entity);

            var modifiedValues = mutator.GetModifiedValues().ToList();

            if (modifiedValues.Count == 0)
                return mutator.Entity;

            foreach (var param in modifiedValues)
                parameters.Add(param.Key, param.Value);

            foreach (var q in BuildUpdateSql(mutator))
                Execute(q, parameters, transaction);

            return mutator.Entity;
        }

        /// <summary>
        /// Updates the records in the database according to mutator
        /// </summary>
        /// <param name="mutator">The mutator which holds the properties that have changed</param>
        /// <param name="transaction"></param>
        /// <returns>Updated entity</returns>
        public TEntity Update(EntityMutator<TEntity> mutator, IDbTransaction transaction = null)
        {
            // if a transaction is specified from the outside,
            // it's the containing code that is responsible for handling it.
            // Therefore, no commit, rollback or dispose on the transaction object.
            bool inTransaction = transaction != null;

            try
            {
                DateTime instance = DateTime.Now;

                if(!inTransaction)
                    transaction = BeginTransaction();

                if(mutator.Entity is IUpdated)
                    mutator.SetProperty("Updated", instance);

                OnBeforeUpdate(mutator, transaction);

                UpdateEntity(mutator, transaction);

                if(!inTransaction)
                    transaction.Commit();

                OnAfterUpdate(mutator.Entity);

                DataContext.OnUpdated(this, mutator.Entity);

                return mutator.Entity;
            }
            catch(Exception ex)
            {
                
                if(!inTransaction)
                    transaction.Rollback();

                DataContext.OnException(this, mutator.Entity, ex);

                throw ex;
            }
            finally
            {
                if(!inTransaction)
                    transaction.Dispose();
            }
        }

        protected virtual void OnBeforeUpdate(EntityMutator<TEntity> mutator, IDbTransaction transaction)
        {

        }

        protected virtual void OnAfterUpdate(TEntity entity)
        {
            UpdateCache(entity);
        }

        #endregion

        #region Create

        protected virtual IEnumerable<string> GetKeys(bool identityOnly = true)
        {
            foreach(var property in typeof(TEntity).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
            {
                var tKeyAttr = property.GetCustomAttributes(typeof(KeyAttribute), false).FirstOrDefault() as KeyAttribute;

                if (tKeyAttr != null && (!identityOnly || tKeyAttr.Type == GeneratedMethod.Database))
                    yield return property.Name;
            }
        }

        // These methods (GetCreateProperties and GetUpdateProperties needs to be checked and simplified.
        // I just put them out like this to move forward in the project, rather than sticking on a detail.
        // TODO : Refactor, simplify and enhance.
        protected virtual string[] GetCreateProperties(Type type, out bool containsKeys)
        {

            List<string> properties = new List<string>();

            containsKeys = false;

            foreach (var property in type.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly))
            {
                var tKeyAttr = property.GetCustomAttributes(typeof(KeyAttribute), false).FirstOrDefault() as KeyAttribute;

                // Database-generated keys should not be added to the create statement
                // There's some missing logic here which needs to be figured out.. Should "containskeys" return true if it's a key but not generated by the database?
                if (tKeyAttr != null && tKeyAttr.Type == GeneratedMethod.Database)
                {
                    containsKeys = true;
                    continue;
                }

                if (property.GetCustomAttributes(typeof(CodeOnlyAttribute), false).Count() > 0)
                    continue;

                properties.Add(property.Name);
            }

            return properties.ToArray();

        }

        private const string CreateSqlFormat = "INSERT INTO [{0}] ({1}) VALUES({2})";

        protected virtual string BuildCreateSql(TEntity entity, string tableName, Type tableType)
        {
            bool keyed;
            IEnumerable<string> props = GetCreateProperties(tableType, out keyed);
            
            // I suspect this is fucky
            if (!keyed)
                props = props.Concat(GetKeys());

            return string.Format(CreateSqlFormat, tableName, string.Join(",", props), string.Join(",", props.Select(s => string.Concat("@", s))));
        }

        /// <summary>
        /// Gets a list of table names for this entity
        /// </summary>
        /// <returns></returns>
        protected IEnumerable<string> GetTableNames()
        {
            var entityType = typeof(TEntity);

            do
            {
                var tAttr = entityType.GetCustomAttributes(typeof(TableAttribute), false).FirstOrDefault() as TableAttribute;

                if (tAttr == null)
                    continue;

                yield return tAttr.Name ?? entityType.Name;


            } while ((entityType = entityType.BaseType) != typeof(object));
        }

        /// <summary>
        /// Creates an SQL Script for creating this entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        protected IEnumerable<string> BuildCreateSql(TEntity entity)
        {
            var entityType = typeof(TEntity);

            do
            {
                var tAttr = entityType.GetCustomAttributes(typeof(TableAttribute), false).FirstOrDefault() as TableAttribute;

                if (tAttr == null)
                    continue;

                yield return BuildCreateSql(entity, tAttr.Name ?? entityType.Name, entityType);


            } while ((entityType = entityType.BaseType) != typeof(object));

        }

        private const string IdentityFormatSqlServer = "SELECT @@IDENTITY AS Id";

        /// <summary>
        /// Updates the entity with the key that was last inserted by the database engine.
        /// This needs to be corrected if you use a database other than SQL Server or SQL Database.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="transaction"></param>
        protected virtual void UpdateEntityKey(TEntity entity, IDbTransaction transaction)
        {
            EntityMutator<TEntity> mutator = new EntityMutator<TEntity>(entity);

            if (GetKeys().Count() == 0)
                return;

            // SQL Server / SQL Database
            mutator.SetProperty(GetKeys().First(), Convert.ChangeType(Query(IdentityFormatSqlServer, null, transaction).First().Id, typeof(TEntity).GetProperty(GetKeys().First()).PropertyType));
            // GetKeys();

            // Mysql / MariaDB
            // mutator.SetProperty(GetKeys().First(), Convert.ChangeType(Query("SELECT LAST_INSERT_ID() AS Id").First().Id, typeof(TEntity).GetProperty(GetKeys().First()).PropertyType));

            // PostgreSql
            // mutator.SetProperty(GetKeys().First(), Convert.ChangeType(Query("SELECT LASTVAL() AS Id").First().Id, typeof(TEntity).GetProperty(GetKeys().First()).PropertyType));
            // Although I doubt Postgre will work without more modifications

            // To get Oracle to work, the insert script needs a slight alteration
            // In Oracle you can use the RETURNING clause; INSERT INTO foo (...) VALUES(...) RETURNING Id into some_id
            // (Postgre also supports this syntax)

            // TODO : Add method for checking database type?
        }

        protected virtual void OnBeforeCreate(TEntity entity, IDbTransaction transaction)
        {
            // Add any code you need to perform on entities before they are written to the database
        }

        protected virtual void OnAfterCreate(TEntity entity)
        {
            UpdateCache(entity);
        }

        /// <summary>
        /// Creates a mutator for entity
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public EntityMutator<TEntity> CreateMutator(TEntity target)
        {
            return new EntityMutator<TEntity>(target);
        }

        protected virtual TEntity CreateEntity(TEntity entity, IDbTransaction transaction = null)
        {
            DynamicParameters parameters = new DynamicParameters();

            // Get parameter values from entity
            parameters.AddDynamicParams(entity);

            // Create a list of queries that needs to be run, and reverse it to get them in the right order (In our case, inheritance works "backwards" compared to what we need)
            var queryList = new Queue<string>(BuildCreateSql(entity).Reverse());

            // The first one will be the initial INSERT operation which probably will give us an Id to work with
            Execute(queryList.Dequeue(), parameters, transaction);

            // Update entity key, if one has been set
            UpdateEntityKey(entity, transaction);

            // Insert into following tables
            for (int i = queryList.Count; i > 0; i--)
                Execute(queryList.Dequeue(), parameters, transaction);

            return entity;
        }

        /// <summary>
        /// Creates an entity in the database
        /// </summary>
        /// <param name="entity">Entity to create</param>
        /// <param name="transaction">Transaction to use, null if no external transaction.</param>
        /// <returns>Entity created</returns>
        public TEntity Create(TEntity entity, IDbTransaction transaction = null)
        {
            
            bool inTransaction = transaction != null;

            DateTime instance = DateTime.Now;

            

            try
            {
                
                
                if (!inTransaction)
                    transaction = BeginTransaction();

                // if the entity has a Created field, set its value to current time
                var icreated = entity as ICreated;
                if (icreated != null && icreated.Created.Equals(default(DateTime)))
                    icreated.Created = instance;

                // if the entity has an Updated field, sit its value to current time
                if (entity is IUpdated)
                    ((IUpdated)entity).Updated = instance;

                // Make any programmatic changes to entity before we create the SQL
                OnBeforeCreate(entity, transaction);

                // Perform the database calls
                CreateEntity(entity, transaction);

                // Perform any "after creation" tasks, such as caching
                OnAfterCreate(entity);

                // Notify the context that an entity has been successfully created
                DataContext.OnCreated(this, entity);

                if(!inTransaction)
                    transaction.Commit();

            }
            catch(Exception ex)
            {
                if(!inTransaction)
                    transaction.Rollback();

                DataContext.OnException(this, entity, ex);

                throw ex;
            }
            finally
            {
                if (!inTransaction)
                    transaction.Dispose();
            }


            return entity;
        }



        #endregion

        #region delete

        private const string DeleteSqlFormat = "DELETE FROM [{0}] WHERE {1}";

        protected virtual string BuildDeleteSql(TEntity entity, string tableName)
        {
            // DELETE FROM Table WHERE Key1 = @Key1 AND Key2 = @Key2
            return string.Format(DeleteSqlFormat,
                tableName,
                string.Join(" AND ", GetKeys(false).Select(s => string.Format("{0} = @{0}", s)))
                );
        }

        protected IEnumerable<string> BuildDeleteSql(TEntity entity)
        {
            var entityType = typeof(TEntity);

            do
            {
                var tAttr = entityType.GetCustomAttributes(typeof(TableAttribute), false).FirstOrDefault() as TableAttribute;

                if (tAttr == null)
                    continue;

                yield return BuildDeleteSql(entity, tAttr.Name ?? entityType.Name);


            } while ((entityType = entityType.BaseType) != typeof(object));
        }

        protected virtual void OnBeforeDelete(TEntity entity, IDbTransaction transaction)
        {

        }

        protected virtual void OnAfterDelete(TEntity entity)
        {
            InvalidateCache(entity);
        }

        /// <summary>
        /// Removes an entity from the database
        /// </summary>
        /// <param name="entity">Entity to remove</param>
        /// <param name="transaction"></param>
        public void Delete(TEntity entity, IDbTransaction transaction = null)
        {
            bool inTransaction = transaction != null;

            try
            {
                if (!inTransaction)
                    transaction = BeginTransaction();

                OnBeforeDelete(entity, transaction);

                DynamicParameters parameters = new DynamicParameters(entity);

                foreach (var sql in BuildDeleteSql(entity))
                    Execute(sql, parameters, transaction);

                if(!inTransaction)
                    transaction.Commit();

                OnAfterDelete(entity);

                DataContext.OnDeleted(this, entity);

            }
            catch(Exception ex)
            {
                if(!inTransaction)
                    transaction.Rollback();

                DataContext.OnException(this, entity, ex);

                throw ex;
            }
            finally
            {
                if(!inTransaction)
                    transaction.Dispose();
            }
        }

        #endregion

        #region Cache

        public virtual ICache Cache { get { return DataContext.Cache; } }

        /// <summary>
        /// Updates or inserts an entity into the cache
        /// </summary>
        /// <param name="entity"></param>
        protected virtual void UpdateCache(TEntity entity)
        {
            if (entity == null)
                return;

            Cache.Insert(GetCacheKey(entity), entity);
        }

        /// <summary>
        /// Invalidates (removes) an entry from the cache
        /// </summary>
        /// <param name="id"></param>
        protected virtual void InvalidateCache(object id)
        {
            Cache.Remove(GetCacheKey(id));
        }

        /// <summary>
        /// Creates a cache key for an object containing properties with the same names as the fields on the table marked with the KeyAttribute
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected virtual string GetCacheKey(object id)
        {
            var keys = GetKeys(false);

            var props = id.GetType().GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance).Where(p => keys.Contains(p.Name)).OrderBy(p => p.Name);

            return string.Format("{0}_[{1}]", typeof(TEntity).Name, string.Join(",", props.Select(a => string.Format("({0}:{1})", a.Name, a.GetValue(id)))));
        }

        /// <summary>
        /// Returns an entity from cache by its key
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected virtual TEntity GetCache(object id)
        {
            return (TEntity)Cache.Get(GetCacheKey(id));
        }

        #endregion

        public virtual void Dispose()
        {
            // In case any inheriting repositories have some corpses that they need to get rid of
        }
    }
}
