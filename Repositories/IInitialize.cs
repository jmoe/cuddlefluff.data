﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuddlefluff.Data.Repositories
{
    /// <summary>
    /// Tells the datacontext that this repository needs to be initialized after it has been created
    /// </summary>
    public interface IInitialize
    {
        void Initialize();
    }
}
