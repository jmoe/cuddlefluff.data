﻿/* Copyright Jarle Moe 2014
 * 
 * http://www.apache.org/licenses/LICENSE-2.0.html
 * 
 */
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuddlefluff.Data.Repositories
{
    /// <summary>
    /// Represents entities that are represented by a single identity colum with the name Id
    /// </summary>
    /// <typeparam name="TEntity">Entitytype to work with. Must implement the interface IIdentifier</typeparam>
    /// <typeparam name="TId">The type of the Id column</typeparam>
    public class IdentityEntityRepository<TEntity, TId> : BaseEntityRepository<TEntity> where TEntity : IIdentifier<TId>, new()
    {

        /// <summary>
        /// Gets an entity by its Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public virtual TEntity GetById(TId id, IDbTransaction transaction = null)
        {
            var result = GetEntityById(new { Id = id }, transaction);

            return result;
        }
        
    }
}
