﻿/* Copyright Jarle Moe 2014
 * 
 * http://www.apache.org/licenses/LICENSE-2.0.html
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuddlefluff.Data.Cache
{
    public interface ICache : IDisposable
    {
        object Get(string key);
        void Insert(string key, object value);
        void Insert(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration);
        void Remove(string key);
    }
}
