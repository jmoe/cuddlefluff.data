﻿/* Copyright Jarle Moe 2014
 * 
 * http://www.apache.org/licenses/LICENSE-2.0.html
 * 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cuddlefluff.Data.DataAnnotations
{
    /// <summary>
    /// Marks an entity as having a field called Created which contains a datetime which will be automatically populated by a repository
    /// </summary>
    public interface ICreated
    {
        /// <summary>
        /// Gets or sets the time this entity was created
        /// </summary>
        DateTime Created { get; set; }
    }
}
