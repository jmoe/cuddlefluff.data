﻿/* Copyright Jarle Moe 2014
 * 
 * http://www.apache.org/licenses/LICENSE-2.0.html
 * 
 */
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Cuddlefluff.Data
{
    /// <summary>
    /// Mutates the property of an entity. This is required so that the repository knows which fields have been altered. Required for update operations
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class EntityMutator<TEntity> where TEntity : new()
    {
        private readonly List<string> _modifiedProperties;
        private readonly TEntity _target;
        
        private static readonly Type _entityType;
        private static readonly List<PropertyInfo> _properties;


        protected IEnumerable<PropertyInfo> TargetProperties
        {
            get
            {
                return _properties.AsEnumerable();
            }
        }

        /// <summary>
        /// Gets the entity target locked to this mutator
        /// </summary>
        public TEntity Entity
        {
            get
            {
                return _target;
            }
        }

        protected Type EntityType
        {
            get
            {
                return _entityType;
            }
        }


        private static IEnumerable<PropertyInfo> GetProperties()
        {
            return _entityType.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
        }

        static EntityMutator()
        {
            _entityType = typeof(TEntity);
            _properties = new List<PropertyInfo>(GetProperties());
        }

        /// <summary>
        /// Instanciates a new mutator
        /// </summary>
        /// <param name="target"></param>
        public EntityMutator(TEntity target)
        {
            _modifiedProperties = new List<string>();
            _target = target;
        }

        /// <summary>
        /// Sets a property and adds it to the list of modified properties
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public virtual EntityMutator<TEntity> SetProperty(string name, object value)
        {
            var property = GetProperties().FirstOrDefault(a => a.Name.Equals(name, StringComparison.OrdinalIgnoreCase));

            // make sure th property exists and that it is writable.
            if (property == null || !property.CanWrite)
                return this;

            var currentValue = property.GetValue(Entity);

            // If the value is identical, skip it.
            if (currentValue != null && currentValue.Equals(value) || value == null && currentValue == null)
                return this;

            _modifiedProperties.Add(property.Name);


            property.SetValue(Entity, value);

            return this;
        }

        /// <summary>
        /// Apply all properties set by a source. The object may be a dynamic
        /// </summary>
        /// <param name="source"></param>
        public EntityMutator<TEntity> Apply(object source)
        {
            if (source == null)
                throw new ArgumentException("Source cannot be null", "source");

            var sourceType = source.GetType();

            foreach(var property in sourceType.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
            {
                if (!property.CanRead)
                    continue;

                string name = property.Name;

                SetProperty(name, property.GetValue(source));

            }

            return this;

        }

        /// <summary>
        /// Gets a list of modified properties
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetModifiedProperties()
        {
            return _modifiedProperties.AsEnumerable();
        }

        /// <summary>
        /// Gets a list of key-value pairs of which properties has been set and what their values are
        /// </summary>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<string, object>> GetModifiedValues()
        {
            foreach(var key in GetModifiedProperties())
            {
                var propertyInfo = EntityType.GetProperty(key, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);

                var name = propertyInfo.Name;

                if (!propertyInfo.CanRead)
                    continue;

                yield return new KeyValuePair<string, object>(name, propertyInfo.GetValue(Entity));
            }
        }

        /// <summary>
        /// Creates a DynamicParameters object which contains all the modified values
        /// </summary>
        /// <returns></returns>
        public DynamicParameters GetParameters()
        {
            DynamicParameters result = new DynamicParameters();

            foreach(var entry in GetModifiedValues())
            {
                result.Add(entry.Key, entry.Value);
            }

            return result;
        }

        public static TEntity Clone(object source)
        {
            var res = new TEntity();

            return new EntityMutator<TEntity>(res).Apply(source).Entity;
        }

    }
}
